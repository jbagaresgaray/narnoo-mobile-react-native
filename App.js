import React from 'react';
import AppContainer from './src/app/app-routing';
export default class AwesomeApp extends React.Component {
  render() {
    return <AppContainer />;
  }
}
