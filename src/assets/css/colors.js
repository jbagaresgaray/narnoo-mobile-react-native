"use strict";

const $colors = {
  primary: "#488aff",
  secondary: "#32db64",
  danger: "#f53d3d",
  light: "#f4f4f4",
  dark: "#222",
  narnoo: "#108fcf",
  share0: "#1091CF",
  share1: "#5FB3DC",
  share2: "#37A1D4",
  share3: "#0578B0",
  share4: "#045881",
  GreyLight: "#EEF5FC",
  google: "#e9142b",
  facebook: "#0346bb",
  twitter: "#00aced",
  flickr: "#ff0084",
  pinterest: "#cb2027",
  linkedin: "#007bb6",
  vimeo: "#1ab7ea",
  tumblr: "#32506d",
  instagram: "#bc2a8d",
  signup: "#045881",
  energized: "#ffc900",
  apperror: "#6f6f6f",
  muted: "#c5c5c5",
  warning: "#fd9a18"
};

export default $colors;
