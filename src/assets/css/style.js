"use strict";
import { StyleSheet } from "react-native";

module.exports = StyleSheet.create({
  alignSelfCenter: {
    alignSelf: "center"
  },
  alignContentCenter: {
    alignContent: "center"
  },
  alignItemsCenter: {
    alignItems: "center"
  },
  textCenter: {
    textAlign: "center"
  },
  textLeft: {
    textAlign: "left"
  },
  textRight: {
    textAlign: "right"
  },
  textJustify: {
    textAlign: "justify"
  },
  margin: {
    margin: 16
  },
  marginLeft: {
    marginLeft: 16
  },
  marginRight: {
    marginRight: 16
  },
  marginTop: {
    marginTop: 16
  },
  marginBottom: {
    marginBottom: 16
  },
  padding: {
    padding: 16
  },
  paddingLeft: {
    paddingLeft: 16
  },
  paddingRight: {
    paddingRight: 16
  },
  paddingTop: {
    paddingTop: 16
  },
  paddingBottom: {
    paddingBottom: 16
  }
});
