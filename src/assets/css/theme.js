"use strict";
import { StyleSheet } from "react-native";
import $colors from "./colors";

module.exports = StyleSheet.create({
  primaryColor: {
    color: $colors.primary
  },
  primaryBgColor: {
    backgroundColor: $colors.primary
  },
  secondaryColor: {
    color: $colors.secondary
  },
  secondaryBgColor: {
    color: $colors.secondary
  },
  dangerColor: {
    color: $colors.danger
  },
  dangerBgColor: {
    backgroundColor: $colors.danger
  },
  lightColor: {
    color: $colors.light
  },
  lightBgColor: {
    backgroundColor: $colors.light
  },
  darkColor: {
    color: $colors.dark
  },
  darkBgColor: {
    backgroundColor: $colors.dark
  },
  narnooColor: {
    color: $colors.narnoo
  },
  narnooBgColor: {
    backgroundColor: $colors.narnoo
  },
  share0Color: {
    color: $colors.share0
  },
  share0BgColor: {
    backgroundColor: $colors.share0
  },
  share1Color: {
    color: $colors.share1
  },
  share1BgColor: {
    backgroundColor: $colors.share1
  },
  share2Color: {
    color: $colors.share2
  },
  share2BgColor: {
    backgroundColor: $colors.share2
  },
  share3Color: {
    color: $colors.share3
  },
  share3BgColor: {
    backgroundColor: $colors.share3
  },
  share4Color: {
    color: $colors.share4
  },
  share4BgColor: {
    backgroundColor: $colors.share4
  },
  GreyLightColor: {
    color: $colors.GreyLight
  },
  GreyLightBgColor: {
    backgroundColor: $colors.GreyLight
  },
  googleColor: {
    color: $colors.google
  },
  googleBgColor: {
    backgroundColor: $colors.google
  },
  facebookColor: {
    color: $colors.facebook
  },
  facebookBgColor: {
    backgroundColor: $colors.facebook
  },
  twitterColor: {
    color: $colors.twitter
  },
  twitterBgColor: {
    backgroundColor: $colors.twitter
  },
  flickrColor: {},
  flickrBgColor: {},
  pinterestColor: {},
  pinterestBgColor: {},
  linkedinColor: {},
  linkedinBgColor: {},
  vimeoColor: {},
  vimeoBgColor: {},
  tumblrColor: {},
  tumblrBgColor: {},
  instagramColor: {},
  instagramBgColor: {},
  signupColor: {},
  signupBgColor: {},
  energizedColor: {},
  energizedBgColor: {},
  apperrorColor: {},
  apperrorBgColor: {},
  mutedColor: {},
  mutedBgColor: {},
  warningColor: {},
  warningBgColor: {}
});
