import React from 'react';
import {Image} from 'react-native';
import {
  Container,
  Header,
  Left,
  Icon,
  Right,
  Button,
  Body,
  Content,
  Text,
} from 'native-base';

export default class DownloadsPage extends React.Component {
  render() {
    return (
      <Container>
        <Header>
          <Left style={{flex: 1}}>
            <Button
              transparent
              onPress={() => this.props.navigation.openDrawer()}>
              <Icon name="menu" />
            </Button>
          </Left>
          <Body style={{flex: 1}}>
            <Text>About</Text>
          </Body>
          <Right style={{flex: 1}} />
        </Header>
        <Content padder>
          <Text style>
            This calculator is intended for general information purposes only;
            to find the approximate length of new, unused belts. The calculator
            should not be used for previously installed belts, as they are
            permanently stretched. The information provided is not intended to
            offer or substitute professional engineer design, review, and
            analysis of the needs of the user's particular application. User is
            solely responsible for determining and evaluating the suitability of
            any belt for the user's particular application. By using the Pulley
            App, the user acknowledges any reliance on the information is at
            your own risk. User further agrees to indemnify and hold harmless
            The Pulley App, LLC, its owners, and employees from any and all
            liability, damages, costs, fees, and expenses.
          </Text>
        </Content>
      </Container>
    );
  }
}
