import React from 'react';
import {Image} from 'react-native';
import {
  Container,
  Button,
  Content,
  Text,
  View,
  Form,
  Item as FormItem,
  Label,
  Input,
  Toast,
} from 'native-base';
import Spinner from 'react-native-loading-spinner-overlay';

import theme from '../../../assets/css/style';
import styles from './login.style';

import {authenticate} from '../../services/login';
import {setLocalStorage, storageKey} from '../../services/storage';

export default class LoginPage extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      users: {
        username: 'wellzie.james@gmail.com',
        password: '12345678',
      },
      spinner: false,
    };
  }

  loginApp() {
    console.log('loginApp');
    this.setState({
      spinner: true,
    });
    console.log('this.state.users: ', this.state.users);
    authenticate(this.state.users)
      .then(data => {
        if (data && data.success) {
          this.setState({
            spinner: false,
          });
          setLocalStorage(
            storageKey.CURRENT_USER,
            JSON.stringify(data.userData),
          );
          setLocalStorage(storageKey.USER_TOKENS, data.token);
          this.props.navigation.navigate('Drawer');
        } else {
          console.log('data.message: ', data.message);
          this.setState({
            spinner: false,
          });
        }
      })
      .catch(error => {
        console.log('request failed', error);
        Toast.show({
          text: 'Error request failed',
          buttonText: 'Okay',
          position: 'bottom',
        });
        this.setState({
          spinner: false,
        });
      });
  }

  render() {
    const loginLogo = require('../../../assets/img/logo.png');
    const bgImage = require('../../../assets/login.png');

    return (
      <Container>
        <Content>
          <Spinner
            visible={this.state.spinner}
            textContent={'Loading...'}
            textStyle={styles.spinnerTextStyle}
          />
          <Image source={bgImage} style={styles.bgContent} />
          <View style={styles.loginContent}>
            <Image source={loginLogo} style={styles.loginLogo} />
            <Form style={styles.form}>
              <FormItem floatingLabel style={theme.lightColor}>
                <Label style={theme.lightColor}>Email</Label>
                <Input
                  value={`${this.state.users.username}`}
                  onChangeText={inputValue => {
                    this.setState(prevState => ({
                      users: {
                        ...prevState.users,
                        username: inputValue,
                      },
                    }));
                  }}
                />
              </FormItem>
              <FormItem floatingLabel last>
                <Label>Password</Label>
                <Input
                  secureTextEntry={true}
                  value={`${this.state.users.password}`}
                  onChangeText={inputValue => {
                    this.setState(prevState => ({
                      users: {
                        ...prevState.users,
                        password: inputValue,
                      },
                    }));
                  }}
                />
              </FormItem>

              <Button
                full
                primary
                style={[theme.marginTop]}
                onPress={() => this.loginApp()}>
                <Text> Login </Text>
              </Button>
              <Button
                full
                primary
                style={[theme.marginTop]}
                onPress={() =>
                  Toast.show({
                    text: 'Wrong password!',
                    buttonText: 'Okay',
                  })
                }>
                <Text> Sign Up </Text>
              </Button>
            </Form>
          </View>
        </Content>
      </Container>
    );
  }
}
