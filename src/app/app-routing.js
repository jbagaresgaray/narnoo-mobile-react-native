import React from 'react';
import {Root} from 'native-base';

import LoginPage from './views/Login/Login';
import HomePage from './views/Home/Home';
import DownloadsPage from './views/Downloads/Downloads';
import UserProfilePage from './views/UserProfile/UserProfile';

import SideBar from './views/Sidebar/Sidebar';

import {createAppContainer} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';
import {createDrawerNavigator} from 'react-navigation-drawer';

const MainDrawerNavigator = createAppContainer(
  createDrawerNavigator(
    {
      Home: {
        screen: HomePage,
      },
      UserProfile: {
        screen: UserProfilePage,
      },
      Downloads: {
        screen: DownloadsPage,
      },
    },
    {
      contentComponent: props => <SideBar {...props} />,
    },
  ),
);

const AppStack = createAppContainer(
  createStackNavigator(
    {
      Login: {
        screen: LoginPage,
        navigationOptions: {
          header: null,
          gesturesEnabled: false,
        },
      },
      Drawer: {
        screen: MainDrawerNavigator,
        navigationOptions: {
          header: null,
          gesturesEnabled: false,
        },
      },
    },
    {
      initialRouteName: 'Login',
      headerMode: 'null',
    },
  ),
);

export default () => (
  <Root>
    <AppStack />
  </Root>
);
