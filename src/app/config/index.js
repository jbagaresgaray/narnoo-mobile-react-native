import {environment} from './environment';

export const baseURL = environment.api_url + environment.api_version;
