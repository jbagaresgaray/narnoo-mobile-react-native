import React from 'react';
import {AppRegistry, Image} from 'react-native';

const remote = '../../assets/login.png';

export default class BackgroundImage extends React.Component {
  render() {
    const resizeMode = 'center';

    return (
      <Image
        style={{
          flex: 1,
          resizeMode,
        }}
        source={{uri: remote}}
      />
    );
  }
}

AppRegistry.registerComponent('BackgroundImage', () => BackgroundImage);
