'use strict';

import {environment} from '../config/environment';
import {baseURL} from '../config/index';
import axios from 'axios';

export const authenticate = data => {
  return new Promise((resolve, reject) => {
    const callbackResponse = resp => {
      console.log('resp: ', resp);
      resolve(resp.data);
    };

    const errorResponse = error => {
      console.log('error: ', error);
      reject(error.error);
    };

    const formData = new FormData();
    formData.append('username', data.username);
    formData.append('password', data.password);
    console.log('formData: ', formData);
    console.log('JSON: ', JSON.stringify(data));

    // axios.defaults.baseURL = baseURL;
    // axios.defaults.timeout = 100000;
    // axios({
    //   url: "login/authenticate",
    //   method: "POST",
    //   data: data,
    //   headers: {
    //     // "Content-Type": `multipart/form-data; boundary=${form._boundary}`,
    //     "Content-Type": "application/x-www-form-urlencoded",
    //     "API-KEY": environment.api_key,
    //     "API-SECRET-KEY": environment.api_secret_key
    //   }
    // }).then(callbackResponse, errorResponse);

    fetch(baseURL + 'login/authenticate', {
      method: 'POST',

      headers: {
        Accept: 'application/json',
        // "Content-Type": "multipart/form-data",
        // "Content-Type": "application/x-www-form-urlencoded",
        'Content-Type': 'application/json',
        'API-KEY': environment.api_key,
        'API-SECRET-KEY': environment.api_secret_key,
      },
      body: JSON.stringify(data),
    })
      .then(callbackResponse)
      .catch(errorResponse);
  });
};
